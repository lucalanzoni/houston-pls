﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public GameObject startScene;
    public GameObject editScene;
    public GameObject gameScene;

    public GameObject answerA;
    public GameObject answerB;
    public GameObject answerC;

    public Image mainScreen;
    public Text conversationText;

    public GameObject[] logObject;

    public Text timerText;

    public GameObject coffe;

    Queue<Evento> eventQueue;
    Queue<float> eventCountdownQueue;

    public float timer = 0;
    public bool isTimeFlowing = false;

    public bool doubleTime = false;

    // Start is called before the first frame update
    void Start()
    {
        //test
        timer = 20;
    }

    // Update is called once per frame
    void Update()
    {
        if (isTimeFlowing)
        {
            if (timer > 0)
            {
                if (doubleTime) timer -= Time.deltaTime * 2;
                else timer -= Time.deltaTime;

                timerText.text = ((int)timer).ToString();
            }
            else
            {
                Debug.Log("Time run out!");
                endLevel();
            }
        }
    }

    public void loadScene(GameObject scene)
    {
        startScene.SetActive(false);
        editScene.SetActive(false);
        gameScene.SetActive(false);

        scene.SetActive(true);

        // if (scene = game scene....
    }

    // carico un livello
    public void loadLevel(int levelIndex)
    {
        loadScene(gameScene);

        Level level;
        switch (levelIndex)
        {
            case 1:
            default:
                level = GameData.firstLevel;
                break;
        }

        timer = level.timer;
        foreach(var evento in level.events)
        {
            eventQueue.Enqueue(evento);
        }

        foreach (var countdown in level.nextEvent)
        {
            eventCountdownQueue.Enqueue(countdown);
        }
    }

    // avvio il gioco
    public void startGame()
    {
        isTimeFlowing = true;
    }

    //fine livello
    public void endLevel()
    {
        isTimeFlowing = false;
    }

    public void doubleTimeActive()
    {
        doubleTime = !doubleTime;
    }

    public void exitGame()
    {
        Application.Quit();
    }
}
