﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Evento
{
    public int imageId;

    public List<string> content;

    public Answer[] answers = new Answer[3];

    public class Answer
    {
        public string content;

        public GameState parameterVariation;

        public string feedback;
    }
}
