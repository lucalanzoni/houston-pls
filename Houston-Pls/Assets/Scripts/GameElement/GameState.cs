﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameState 
{

    public int radiation;

    public int muscle;

    public int blood;

    public int sight;

    public int psych;

    public int coordination;

}
