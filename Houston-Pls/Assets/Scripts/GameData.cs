﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameData 
{
    public static Evento.Answer firstEventAnswer1 = new Evento.Answer() { content = "test answer1", parameterVariation = new GameState() { blood = 1 }, feedback = "feedback1" };
    public static Evento.Answer firstEventAnswer2 = new Evento.Answer() { content = "test answer2", parameterVariation = new GameState() { coordination = 2 }, feedback = "feedback2" };
    public static Evento.Answer firstEventAnswer3 = new Evento.Answer() { content = "test answer3", parameterVariation = new GameState() { sight = 3 }, feedback = "feedback3" };
    public static Evento firstEvent = new Evento() { imageId = 0, content = { "test1", "test2" }, answers = new Evento.Answer[] { firstEventAnswer1, firstEventAnswer2, firstEventAnswer3 } } ;

    public static Level firstLevel = new Level() { events = new List<Evento>() { firstEvent }, nextEvent = new List<float>() { 3 }, timer = 300f };
}
